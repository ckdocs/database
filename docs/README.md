# Intro

**Algorithm** is the way of solving a **Problem**

In this tutorial we learn about:

1. **Basics of algorithms**
2. **Find an algorithm for a given problem**
3. **Common problems**
4. **Compare algorithms**
5. **Improve algorithms**

---

## Dependencies

1. **Arithmatic**:
    1. `Series`
    2. `Logarithm`
    3. `Growth Of Function`
2. **Mathematical Logic**:
    1. `Mathematical Induction`
3. **Computability Theory**:
    1. `Asymptotic Analysis`
    2. `Amortized Analysis`
    3. `Average Case Analysis`
4. **Combinatorial Theory**:
    1. `Recurrence Relations`
5. **Data Structure**

---

## Problem

When we have some **Existed Inputs** and we want to convert them to some **Deired Outputs**

So the combination of them called a **Problem**:

1. **Existed Inputs**
2. **Desired outputs**

---

**Example**: We want to clean our teethes:

1. **Input**: Dirty tooth
2. **Output**: Clean tooth

---

### Standard Problem

When our inputs can be in any **Size**, the all possible inputs can be **Infinite**, we call these problems **Standard Problem**:

1. **Input**: $\infty$
2. **Output**: $k$

When our problem isn't standard all the possible inputs are **Finite**, so we can use **IF** for them an solve problem in **O(1)** complexity

---

**Example**: Give a chess 8x8 page and tell weather the wite wins or not?

The inputs are **Finite**: $13^{64}$

So the problem can solve in a constant time:

```js
if (chessPage == ...) {
    return true;
}
if (chessPage == ...) {
    return true;
}
if (chessPage == ...) {
    return true;
}
...
```

---

## Solve

There are many ways to solve a problem:

1. Heuristic
2. Algorithm
3. ...

---

### Heuristic

A **mental shortcut** that allows people to solve problems and make judgments **quickly** and **efficiently**

1. A huristic solution may won't be **clear**
2. It hasn't **step-by-step** procedure
3. It cannot **implement** in computers
4. It's **fast**
5. It may be have **error**

---

### Algorithm

**Algorithm** is a `step-by-step` **procedure**, which defines a set of `instructions` to be executed in a certain order to get the `desired output`:

1. **Input**: should have `0` or `more` well-defined inputs
2. **Output**: should have `1` or `more` well-defined outputs
3. **Pure**: should not have any `side-effect`, all dependencies must `get as input`
4. **Halt**: should halt (end) after `finite` number of `steps`
5. **Independent**: should be only `logic` independent of `language`, can implement on any language
6. **Unambiguous**: the logic of algorithm should be `clear` and `well-defined`

---

**Example**:

-   **Problem**: How to clean toothes (We need a machine to clean toothes)
    -   **Input**: Dirty tooth
    -   **Output**: Clean tooth
-   **Algorith**:
    1. Wash Toothbrush
    2. Brush toothpaste on the toothbrush
    3. Brushing your teeth
    4. Wash your mouth
    5. Wash your toothbrush

---

## Compare Solutions

We design an algorithm to get a **solution** of a given **problem**. A problem can be solved in **more than one** ways.

![Problem Solutions](/assets/problem_solutions.jpg)

For finding the best **Solution** (**Algorithm**) for our problem, we must find **Efficiency** of algorithms and **Compare** then

Algorithms for one **problem** may differ in **Time** and **Space** required, depends on the **Limitations** we select the better algorithm, for example **Bubble Sort** and **Quick Sort**:

| Bubble Sort     | Quick Sort       |
| --------------- | ---------------- |
| **Higher Time** | **Lower Time**   |
| **Lower Space** | **Higher Space** |

-   If our memory is limited, we can use **Bubble Sort**
-   If the speed is important, we can use **Quick Sort**

---

## Algorithm vs Data Structure

The `main` (**Not Entire**) goal of **Algorithms** is to operate on **Data Structures**, in this view these are the most important categories of algorithms:

1. **Search**
2. **Sort**
3. **Insert**
4. **Update**
5. **Delete**

---
